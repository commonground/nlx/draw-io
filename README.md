# NLX-Drawio Repository

This repository is part of the NLX information and holds the graphical architecture models and exports of the models for reuse in documentation and presentations.

### An overview of the published models:

1. The basic NLX model

![basic NLX model](https://gitlab.com/commonground/nlx/draw-io/raw/master/published/nlx%20basic-NLX%20Basic.svg)

2. The basic NLX Authorization model
   
![basic NLX Authorization model](https://gitlab.com/commonground/nlx/draw-io/raw/master/published/nlx%20basic-authorization.svg)

3. The basic PKI-Overheid NLX View

![basic PKI-Overheid NLX View](https://gitlab.com/commonground/nlx/draw-io/raw/master/published/NLX-PKIOverheid-Basic%20view.svg) 

### An overview of models currently under development

1. The NLX Component view

![nlx component view](concepts/NLX-Component-overview.svg)

2. The NLX Component certificate view
   
![nlx component certificate view](concepts/NLX-Certificate-overview.svg)


### More information about Draw.io is available [here](https://www.draw.io/)
